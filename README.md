# uexSDPMerchantPay

## 简介

封装盛付通插件功能


## 方法

###  onEvent 

**定义**

`startPayWithOrderInfo(params)`

**说明**

* 支付功能


**参数**

| 参数名称       | 参数类型   | 是否必选 | 说明    |
| ---------- | ------ | ---- | ----- | 
| params  | JSON | 是    | 参数 | 

**示例**

```javascript

var privateKeyStr = "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDbODPX+TrymksM5+aRF3K5b/cDT+fDq5GKfgGlZO8MlgEG4UpOEh097UYIYrWEzBQs53MDn3dIwlMt4X+4FuKcECG3+drZ3Ehzh68qDak+q/2pIiEb0QfdCULY8Xz3TIoN0EKnCCVoHkHo3+Vel3uaUtlU74bnB0CCw1jGWdJOSeEpUiU5MjujjyzYbOVsz4AdzbmEfZFB8e+mSsM7zjcriyUponkMSXBKIiDQehKn0YLp39N2sJrpG/2SOcZnPh3wc/k6OtJP52CHVMDjrB4wTdf6njiqntGub+nuiIJDDGl+4HzahVfQ5IruMbWThRQzUC+KF0Q2tIFIMc0XuXfJAgMBAAECggEBALUXFmyg67shDkJZBzRwREs/XLQVvAT9o7reIIn6eSbSe4KtdO5NNG7FpQakVAKhe0Ek6PPjNWybao8KKrFt5kC5asFJ2yoBOLCHM4HvyxGEjoS7NtJ9uJs1XU1NH8hCKAEFOyo0JoJ+DEBNRHMBfA+dxP7O54fNi9L5gEpKRNp50ePm20H5m2rHRu6nhOloAJZA+jXog3Yc2283Bnjx1PwpkP8LZypC+TodIOx3aZMRoXYSz6BRia8ylar1tr2Y8oJoOV8ENWGp6z0tj/YH/Cwc9jYHlHJWcMdzkTmlohwAiJmsFjRAsn8ognhmHoDFplaWM7Zlof5AhNSDXRwdHYkCgYEA7/dL9f3M0ArdfutBmx67O1l48HsAUG/O8dX49oVoCZI+91C/NyQ0r1A6iu9qjSvmIPBmbIOeGH0kRbl4BwRGF1j1wVB2rgl5FFf5oYs6TPvgGR9losvd4Bk3UOsua9nUkpmHOE/6ONZYGo2PhpYRhTc5/pOMW7jEW0UgicJd2e8CgYEA6d4HvU4nx8jWhFo4IDmb9lE6ccoG+6gUGSxkzQZUtfa+H/EsFlQv3GNGvXNQ/oQ3oAQQrFxfvOSQ51MXED2MOKluGqy7DSSkp2ocRfFU7wEbordU3rhfKMROsj1NYMj4anUWT4AnsVc8JezatKViWbXmwuyk1plovjaNWEFm4ccCgYA6nDUybP4EZlL5N+67O4NRmKXgXrqR6u0pxjBbzfO+OrkkYNWDW1V+6GKUIqvstSctLmpl4LPRmWctnIJDfHi+JR1JTSTflzK6lE5FdaMUwIRYvoFthMu5e482NWsOLpMsB1GuoGImVbwJKEBBCBBZcEa69kDW+kcDX9v1qcKQgwKBgQDkzNE7lpv6rHWa1P02IcaBDGUmcCW2zXCkVDdmEnyL7ZOCgpvEWKbecc4CiTBDYS1egruhNVqA3gkaF8Nnox9tS+2pcTYrHJ9uHrT8hKe4kJft8Hi60RxgMPZhEPKD7vqChHzIWLP8n0D8RaaOt4LqC7lxGL4IKdw8w/gy0QGLfwKBgQDN4POsfAgXieHkFZdOoc9++memQFgYQHVgfrepAw2MZxV+YX/75FkymHxKrONCAqFs7bS1u2GfPRpkpiYbyHmboAkBgK3AiUYExpBUs27ZjwP3FSNLYxT9Pe4f/tOXpmSpZnaiXvUJ16OPCBLRHf77+C8DQ8NLnVCkSGdW4/nd/w==";
    
    
    var json = {
        privateKey:privateKeyStr,//秘钥
        merchantNo:"540506",//商户号
        charset:"UTF-8",//字符集
        requestTime:"20170915121348",//请求时间
        outMemberId:"out245888",//商户会员标识
        outMemberRegistTime:"20170915121348",//商户会员注册时间
        outMemberRegistIP:"127.0.0.1",//商户会员注册IP
        outMemberVerifyStatus:"0",//商户会员是否已实名
        outMemberName:"张三",//商户会员注册姓名
        outMemberMobile:"13841552142",//商户会员注册手机号
        merchantOrderNo:"20170915121348",//商户订单号
        productName:"iPhone7 128G 土豪金",//商品名称
        productDesc:"iphone手机,iphone6s",//商品描述
        currency:"CNY",//货币类型
        amount:"0.01",//交易金额
        notifyUrl:"http://10.132.97.38:8088/html5-gateway-general/express.jsp",//后台通知回调地址
        userIP:"188.178.1.200",//用户IP
        bankCardType:"",//银行卡类型
        bankCode:"",//机构代码
        extsDict:{//扩展字段
            uniqueName:"方大山",//姓名
            uniqueIDNo:"441322194508081119",
            uniqueCardNo:"6226221233211222" //卡号
        }
    }
    
    
    uexSDPMerchantPay.startPayWithOrderInfo(json);

```

