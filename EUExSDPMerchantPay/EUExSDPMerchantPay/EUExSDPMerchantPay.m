//
//  EUExSDPMerchantPay.m
//  EUExSDPMerchantPay
//
//  Created by liguangqiao on 2017/9/11.
//  Copyright © 2017年 plugin. All rights reserved.
//

#import "EUExSDPMerchantPay.h"
#import "SDPMerchantPay.h"
#import "HBRSAHandler.h"
#import "WidgetOneDelegate.h"

//#define PrivateKey @"MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDbODPX+TrymksM5+aRF3K5b/cDT+fDq5GKfgGlZO8MlgEG4UpOEh097UYIYrWEzBQs53MDn3dIwlMt4X+4FuKcECG3+drZ3Ehzh68qDak+q/2pIiEb0QfdCULY8Xz3TIoN0EKnCCVoHkHo3+Vel3uaUtlU74bnB0CCw1jGWdJOSeEpUiU5MjujjyzYbOVsz4AdzbmEfZFB8e+mSsM7zjcriyUponkMSXBKIiDQehKn0YLp39N2sJrpG/2SOcZnPh3wc/k6OtJP52CHVMDjrB4wTdf6njiqntGub+nuiIJDDGl+4HzahVfQ5IruMbWThRQzUC+KF0Q2tIFIMc0XuXfJAgMBAAECggEBALUXFmyg67shDkJZBzRwREs/XLQVvAT9o7reIIn6eSbSe4KtdO5NNG7FpQakVAKhe0Ek6PPjNWybao8KKrFt5kC5asFJ2yoBOLCHM4HvyxGEjoS7NtJ9uJs1XU1NH8hCKAEFOyo0JoJ+DEBNRHMBfA+dxP7O54fNi9L5gEpKRNp50ePm20H5m2rHRu6nhOloAJZA+jXog3Yc2283Bnjx1PwpkP8LZypC+TodIOx3aZMRoXYSz6BRia8ylar1tr2Y8oJoOV8ENWGp6z0tj/YH/Cwc9jYHlHJWcMdzkTmlohwAiJmsFjRAsn8ognhmHoDFplaWM7Zlof5AhNSDXRwdHYkCgYEA7/dL9f3M0ArdfutBmx67O1l48HsAUG/O8dX49oVoCZI+91C/NyQ0r1A6iu9qjSvmIPBmbIOeGH0kRbl4BwRGF1j1wVB2rgl5FFf5oYs6TPvgGR9losvd4Bk3UOsua9nUkpmHOE/6ONZYGo2PhpYRhTc5/pOMW7jEW0UgicJd2e8CgYEA6d4HvU4nx8jWhFo4IDmb9lE6ccoG+6gUGSxkzQZUtfa+H/EsFlQv3GNGvXNQ/oQ3oAQQrFxfvOSQ51MXED2MOKluGqy7DSSkp2ocRfFU7wEbordU3rhfKMROsj1NYMj4anUWT4AnsVc8JezatKViWbXmwuyk1plovjaNWEFm4ccCgYA6nDUybP4EZlL5N+67O4NRmKXgXrqR6u0pxjBbzfO+OrkkYNWDW1V+6GKUIqvstSctLmpl4LPRmWctnIJDfHi+JR1JTSTflzK6lE5FdaMUwIRYvoFthMu5e482NWsOLpMsB1GuoGImVbwJKEBBCBBZcEa69kDW+kcDX9v1qcKQgwKBgQDkzNE7lpv6rHWa1P02IcaBDGUmcCW2zXCkVDdmEnyL7ZOCgpvEWKbecc4CiTBDYS1egruhNVqA3gkaF8Nnox9tS+2pcTYrHJ9uHrT8hKe4kJft8Hi60RxgMPZhEPKD7vqChHzIWLP8n0D8RaaOt4LqC7lxGL4IKdw8w/gy0QGLfwKBgQDN4POsfAgXieHkFZdOoc9++memQFgYQHVgfrepAw2MZxV+YX/75FkymHxKrONCAqFs7bS1u2GfPRpkpiYbyHmboAkBgK3AiUYExpBUs27ZjwP3FSNLYxT9Pe4f/tOXpmSpZnaiXvUJ16OPCBLRHf77+C8DQ8NLnVCkSGdW4/nd/w=="//生产环境下的私钥

@implementation EUExSDPMerchantPay

#pragma mark - Life Cycle

- (instancetype)initWithWebViewEngine:(id<AppCanWebViewEngineObject>)engine{
    self = [super initWithWebViewEngine:engine];
    if (self) {
        ACLogDebug(@"插件实例被创建");
    }
    return self;
}


- (void)clean{
    
    ACLogDebug(@"网页即将被销毁");
}


- (void)startPayWithOrderInfo:(NSMutableArray *)inArguments{
   
    
    if([inArguments count] < 1){
        //当传入的参数为空时，直接返回，避免数组越界错误。
        return;
    }
    
    ACArgsUnpack(NSDictionary *infoDic) = inArguments;

    
    if (infoDic == nil) {
        return;
    }
    
    
    
//    NSDate *currentDate = [NSDate date];
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"yyyyMMddHHmmss"];
//    NSString *dateStr = [formatter stringFromDate:currentDate];
    
    
    SDPOrderInfo *orderInfo = [[SDPOrderInfo alloc] init];
    
    orderInfo.merchantNo =  infoDic[@"merchantNo"];
    orderInfo.charset = infoDic[@"charset"];
    orderInfo.requestTime = infoDic[@"requestTime"];
    orderInfo.outMemberId = infoDic[@"outMemberId"];
    orderInfo.outMemberRegistTime = infoDic[@"outMemberRegistTime"];
    orderInfo.outMemberRegistIP = infoDic[@"outMemberRegistIP"];
    orderInfo.outMemberVerifyStatus = infoDic[@"outMemberVerifyStatus"];
    orderInfo.outMemberName = infoDic[@"outMemberName"];
    orderInfo.outMemberMobile = infoDic[@"outMemberMobile"];
    orderInfo.merchantOrderNo = infoDic[@"merchantOrderNo"];
    orderInfo.productName = infoDic[@"productName"];
    orderInfo.productDesc = infoDic[@"productDesc"];
    orderInfo.currency = infoDic[@"currency"];
    orderInfo.amount = infoDic[@"amount"];
    orderInfo.notifyUrl = infoDic[@"notifyUrl"];
    orderInfo.userIP = infoDic[@"userIP"];
    orderInfo.bankCardType = infoDic[@"bankCardType"];
    orderInfo.bankCode = infoDic[@"bankCode"];
    
    
    //第一版先不加内部会员和跨境商户
    NSDictionary *extsDict = infoDic[@"extsDict"];
    
//    orderInfo.merchantNo =  @"540506";
//    orderInfo.charset = @"UTF-8";
//    orderInfo.requestTime = dateStr;
//    orderInfo.outMemberId = @"out245888";
//    orderInfo.outMemberRegistTime = dateStr;
//    orderInfo.outMemberRegistIP = @"127.0.0.1";
//    orderInfo.outMemberVerifyStatus = @"0";
//    orderInfo.outMemberName = @"张三";
//    orderInfo.outMemberMobile = @"13841552142";
//    orderInfo.merchantOrderNo = dateStr;
//    orderInfo.productName = @"iPhone7 128G 土豪金";
//    orderInfo.productDesc = @"iphone手机,iphone6s";
//    orderInfo.currency = @"CNY";
//    orderInfo.amount = @"0.01";
//    orderInfo.notifyUrl = @"http://10.132.97.38:8088/html5-gateway-general/express.jsp";
//    orderInfo.userIP = @"188.178.1.200";
//    orderInfo.bankCardType = @"";
//    orderInfo.bankCode = @"";
//    
//    
//    //第一版先不加内部会员和跨境商户
//    NSDictionary *extsDict = @{
//                               @"uniqueName":@"方大山",
//                               @"uniqueIDNo":@"441322194508081119",
//                               @"uniqueCardNo":@"6226221233211222",
//                               };
    //    NSDictionary *extsDict = @{
    //                               @"userId":_userIdTextField.text,
    //                               @"userIdType":_userIdTypeTextField.text,
    //                               @"uniqueName":_uniqueNameTextField.text,
    //                               @"uniqueIDNo":_uniqueIDNoTextField.text,
    //                               @"uniqueCardNo":_uniqueCardNoTextField.text,
    //                               @"bizRealName":_bizRealNameTextField.text,
    //                               @"bizIdNo":_bizIdNoTextField.text,
    //                               @"bizMobile":_bizMobileTextField.text
    //                               };
    NSData *extsData = [NSJSONSerialization dataWithJSONObject:extsDict options:NSJSONWritingPrettyPrinted error:nil];
    NSString *extsStr = [[NSString alloc] initWithData:extsData encoding:NSUTF8StringEncoding];
    orderInfo.exts = extsStr;
    
    NSString *privateKey = infoDic[@"privateKey"];
    
    orderInfo.signType = @"RSA";
    orderInfo.signMsg = [self createSignMsgWithOrderInfo:orderInfo privateKey:privateKey];
    
    SDPMerchantPay *merchantPay = [SDPMerchantPay new];
    [merchantPay startPayWithOrderInfo:orderInfo inWindow:theApp.window];

}

#pragma mark - 拼接orderInfo中signMsg
- (NSString *)createSignMsgWithOrderInfo:(SDPOrderInfo *)orderInfo privateKey:(NSString*)privateKey {
    //原始数据
    NSMutableString *originString = [NSMutableString stringWithString:@""];
    if (orderInfo.merchantNo && ![orderInfo.merchantNo isEqualToString:@""]) {
        [originString appendString:[NSString stringWithFormat:@"%@|",orderInfo.merchantNo]];
    }
    
    if (orderInfo.charset && ![orderInfo.charset isEqualToString:@""]) {
        [originString appendString:[NSString stringWithFormat:@"%@|",orderInfo.charset]];
    } else {
        [originString appendString:@"UTF-8|"];
    }
    
    if (orderInfo.requestTime && ![orderInfo.requestTime isEqualToString:@""]) {
        [originString appendString:[NSString stringWithFormat:@"%@|",orderInfo.requestTime]];
    }
    
    if (orderInfo.outMemberId && ![orderInfo.outMemberId isEqualToString:@""]) {
        [originString appendString:[NSString stringWithFormat:@"%@|",orderInfo.outMemberId]];
    }
    
    if (orderInfo.outMemberRegistTime && ![orderInfo.outMemberRegistTime isEqualToString:@""]) {
        [originString appendString:[NSString stringWithFormat:@"%@|",orderInfo.outMemberRegistTime]];
    }
    
    if (orderInfo.outMemberRegistIP && ![orderInfo.outMemberRegistIP isEqualToString:@""]) {
        [originString appendString:[NSString stringWithFormat:@"%@|",orderInfo.outMemberRegistIP]];
    }
    
    if (orderInfo.outMemberVerifyStatus && ![orderInfo.outMemberVerifyStatus isEqualToString:@""]) {
        [originString appendString:[NSString stringWithFormat:@"%@|",orderInfo.outMemberVerifyStatus]];
    }
    
    if (orderInfo.outMemberName && ![orderInfo.outMemberName isEqualToString:@""]) {
        [originString appendString:[NSString stringWithFormat:@"%@|",orderInfo.outMemberName]];
    }
    
    if (orderInfo.outMemberMobile && ![orderInfo.outMemberMobile isEqualToString:@""]) {
        [originString appendString:[NSString stringWithFormat:@"%@|",orderInfo.outMemberMobile]];
    }
    
    if (orderInfo.merchantOrderNo && ![orderInfo.merchantOrderNo isEqualToString:@""]) {
        [originString appendString:[NSString stringWithFormat:@"%@|",orderInfo.merchantOrderNo]];
    }
    
    if (orderInfo.productName && ![orderInfo.productName isEqualToString:@""]) {
        [originString appendString:[NSString stringWithFormat:@"%@|",orderInfo.productName]];
    }
    
    if (orderInfo.productDesc && ![orderInfo.productDesc isEqualToString:@""]) {
        [originString appendString:[NSString stringWithFormat:@"%@|",orderInfo.productDesc]];
    }
    
    if (orderInfo.currency && ![orderInfo.currency isEqualToString:@""]) {
        [originString appendString:[NSString stringWithFormat:@"%@|",orderInfo.currency]];
    } else {
        [originString appendString:@"CNY|"];
    }
    
    if (orderInfo.amount && ![orderInfo.amount isEqualToString:@""]) {
        [originString appendString:[NSString stringWithFormat:@"%@|",orderInfo.amount]];
    }
    
    if (orderInfo.notifyUrl && ![orderInfo.notifyUrl isEqualToString:@""]) {
        [originString appendString:[NSString stringWithFormat:@"%@|",orderInfo.notifyUrl]];
    }
    
    if (orderInfo.userIP && ![orderInfo.userIP isEqualToString:@""]) {
        [originString appendString:[NSString stringWithFormat:@"%@|",orderInfo.userIP]];
    }
    
    if (orderInfo.bankCardType && ![orderInfo.bankCardType isEqualToString:@""]) {
        [originString appendString:[NSString stringWithFormat:@"%@|",orderInfo.bankCardType]];
    }
    
    if (orderInfo.bankCode && ![orderInfo.bankCode isEqualToString:@""]) {
        [originString appendString:[NSString stringWithFormat:@"%@|",orderInfo.bankCode]];
    }
    
    if (orderInfo.exts && ![orderInfo.exts isEqualToString:@""]) {
        [originString appendString:[NSString stringWithFormat:@"%@|",orderInfo.exts]];
    }
    
    if (orderInfo.signType && ![orderInfo.signType isEqualToString:@""]) {
        [originString appendString:[NSString stringWithFormat:@"%@|",orderInfo.signType]];
    }
    
    HBRSAHandler *handler = [[HBRSAHandler alloc] init];
    [handler importKeyWithType:KeyTypePrivate andkeyString:privateKey];
    NSString *signMsg = [handler signMD5String:originString];
    
    return signMsg;
}

@end
