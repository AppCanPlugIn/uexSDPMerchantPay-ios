//
//  SDPMerchantPay.h
//  SDPMerchantPay
//
//  Created by huang jason on 16/9/18.
//  Copyright © 2016年 huang jason. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDPOrderInfo.h"

@interface SDPMerchantPay : NSObject

// 提交订单信息，开始支付
- (void)startPayWithOrderInfo:(nonnull SDPOrderInfo *)orderInfo inWindow:(nonnull UIWindow *)window;

@end
