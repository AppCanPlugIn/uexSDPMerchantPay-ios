//
//  SDPOrderInfo.h
//  SDPMerchantSDK
//
//  Created by huang jason on 16/9/13.
//  Copyright © 2016年 www.shengpay.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SDPOrderInfo : NSObject

@property (nonatomic,copy)  NSString *merchantNo;//商户号
@property (nonatomic,copy)  NSString *charset; //字符集
@property (nonatomic,copy)  NSString *requestTime;//请求时间
@property (nonatomic,copy)  NSString *outMemberId;//商户会员标识
@property (nonatomic,copy)  NSString *outMemberRegistTime;//商户会员注册时间
@property (nonatomic,copy)  NSString *outMemberRegistIP;//商户会员注册IP
@property (nonatomic,copy)  NSString *outMemberVerifyStatus;//商户会员是否已实名
@property (nonatomic,copy)  NSString *outMemberName;//商户会员注册姓名
@property (nonatomic,copy)  NSString *outMemberMobile;//商户会员注册手机号
@property (nonatomic,copy)  NSString *merchantOrderNo;//商户订单号
@property (nonatomic,copy)  NSString *productName;//商品名称
@property (nonatomic,copy)  NSString *productDesc;//商品描述
@property (nonatomic,copy)  NSString *currency;//货币类型
@property (nonatomic,copy)  NSString *amount;//交易金额
@property (nonatomic,copy)  NSString *notifyUrl;//后台通知回调地址
@property (nonatomic,copy)  NSString *userIP;//用户IP
@property (nonatomic,copy)  NSString *bankCardType;//银行卡类型
@property (nonatomic,copy)  NSString *bankCode;//机构代码
@property (nonatomic,copy)  NSString *exts;//扩展字段
@property (nonatomic,copy)  NSString *signType;//签名类型
@property (nonatomic,copy)  NSString *signMsg;//签名消息

@end
