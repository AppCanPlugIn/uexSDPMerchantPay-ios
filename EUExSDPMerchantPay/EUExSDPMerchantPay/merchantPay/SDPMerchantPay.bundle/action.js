var command = (function() {

	var _this = this;


	var commands = {

		//插入tr
		insertTr: function(options) {

			var tab = document.querySelector("#" + options.id);
			var tr = tab.querySelector(".no-bl");
			//console.log(tr);
			var clone = tr.cloneNode(true);
			clone.classList.remove("no-bl");
			var tds = clone.querySelectorAll("td");

			for (var i = 0; i < tds.length; i++) {
				tds[i].innerHTML = options.data[i]
			}

			if (tab.querySelector("tbody")) {
				tab.querySelector("tbody").insertBefore(clone, tr);
			} else {
				tab.insertBefore(clone, tr);
			}
		}

	}


	return function(cmd) {
		///var _this = this;
		commands[cmd].apply(_this,[].slice.call(arguments,1));
	}

})();
