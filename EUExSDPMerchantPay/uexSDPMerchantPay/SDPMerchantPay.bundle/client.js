
(function () {
    document.addEventListener('DOMContentLoaded', function () {
        var html = document.documentElement;
        var windowWidth = html.clientWidth;
        html.style.fontSize = windowWidth / 10.8 + 'px';
        // 等价于html.style.fontSize = windowWidth / 1080 * 100 + 'px';
    }, false);
})();